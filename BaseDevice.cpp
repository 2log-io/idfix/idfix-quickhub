#include "BaseDevice.h"

#include "Connection.h"
#include "TaskManager.h"
#include "IDFixTask.h"

#include "FirmwareUpdater.h"
#include "PublicKey.h"
#include "HashSHA256.h"
#include "ECDSASignatureVerifier.h"
#include "HTTPFirmwareDownloader.h"
#include "DeviceProperties.h"
#include <esp_wifi.h>
#include "MutexLocker.h"

using namespace IDFix;

extern "C"
{
    #include <nvs_flash.h>
}

namespace
{
    const char* LOG_TAG = "_2log::BaseDevice";
    const char* SERVER_CONFIG_PARAMETER = "server";
    const char* TEST_CONFIG_PARAMETER   = "testconfig";

    extern "C"
    {
        extern const unsigned char deviceCertificate[] asm("_binary_ipcertdummy_crt_start");
        extern const unsigned char deviceCertificateEnd[]   asm("_binary_ipcertdummy_crt_end");
        static long deviceCertificateSize = deviceCertificateEnd - deviceCertificate;

        extern const unsigned char deviceKey[] asm("_binary_ipcertdummy_key_start");
        extern const unsigned char deviceKeyEnd[]   asm("_binary_ipcertdummy_key_end");
        static long deviceKeySize = deviceKeyEnd - deviceKey;
    }


#if ALLOW_3RD_PARTY_FIRMWARE == 1

    #warning "3rd party firmware allowed"

    // Open firmware key
    unsigned char firmwareSignaturePublicKey[] =
    {
      0x30, 0x56, 0x30, 0x10, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02,
      0x01, 0x06, 0x05, 0x2b, 0x81, 0x04, 0x00, 0x0a, 0x03, 0x42, 0x00, 0x04,
      0x71, 0x91, 0xa9, 0xda, 0x8c, 0xa1, 0x0c, 0x71, 0xe2, 0x2a, 0x98, 0xc0,
      0x3e, 0x64, 0xdc, 0xf0, 0x81, 0xc9, 0xb9, 0xc8, 0x37, 0xd3, 0xee, 0xe4,
      0xa1, 0x08, 0x0b, 0x89, 0x46, 0x47, 0x11, 0x33, 0xaa, 0x11, 0x4e, 0xac,
      0xfe, 0x6e, 0xff, 0x60, 0xa3, 0xa4, 0x11, 0x1a, 0x10, 0x2f, 0x9c, 0x8c,
      0x7d, 0xfb, 0xf4, 0xc0, 0x6a, 0x48, 0x24, 0x40, 0x43, 0xeb, 0x06, 0x0b,
      0xee, 0xd3, 0x5f, 0xf9
    };

    unsigned int firmwareSignaturePublicKeyLength = 88;

#else

    // Open firmware key TODO: replace with 2log secure firmware key
    unsigned char firmwareSignaturePublicKey[] =
    {
      0x30, 0x56, 0x30, 0x10, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02,
      0x01, 0x06, 0x05, 0x2b, 0x81, 0x04, 0x00, 0x0a, 0x03, 0x42, 0x00, 0x04,
      0x71, 0x91, 0xa9, 0xda, 0x8c, 0xa1, 0x0c, 0x71, 0xe2, 0x2a, 0x98, 0xc0,
      0x3e, 0x64, 0xdc, 0xf0, 0x81, 0xc9, 0xb9, 0xc8, 0x37, 0xd3, 0xee, 0xe4,
      0xa1, 0x08, 0x0b, 0x89, 0x46, 0x47, 0x11, 0x33, 0xaa, 0x11, 0x4e, 0xac,
      0xfe, 0x6e, 0xff, 0x60, 0xa3, 0xa4, 0x11, 0x1a, 0x10, 0x2f, 0x9c, 0x8c,
      0x7d, 0xfb, 0xf4, 0xc0, 0x6a, 0x48, 0x24, 0x40, 0x43, 0xeb, 0x06, 0x0b,
      0xee, 0xd3, 0x5f, 0xf9
    };

    unsigned int firmwareSignaturePublicKeyLength = 88;

#endif

#if DISABLE_TLS_CA_VALIDATION == 1

    #warning "!!!WARNING: TLS VALIDATION DISABLED!!!11elf"

    const char* ROOT_CA = nullptr;

#else

    const char* ROOT_CA =       /* Let's encrypt root CA ISRG Root X1.pem */
		"-----BEGIN CERTIFICATE-----\n\
		MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/\n\
		MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n\
		DkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1ow\n\
		TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\n\
		cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEB\n\
		AQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XC\n\
		ov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpL\n\
		wYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+D\n\
		LtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK\n\
		4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5\n\
		bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5y\n\
		sR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZ\n\
		Xmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4\n\
		FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBc\n\
		SLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2ql\n\
		PRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TND\n\
		TwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYw\n\
		SwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1\n\
		c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx\n\
		+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEB\n\
		ATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQu\n\
		b3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9E\n\
		U1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26Ztu\n\
		MA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC\n\
		5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW\n\
		9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuG\n\
		WCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9O\n\
		he8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFC\n\
		Dfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5\n\
		-----END CERTIFICATE-----\n";
#endif

}

#if OVERRIDE_CONFIG == 1
    #warning "Config override enabled!"
#endif

#if DISABLE_ENERGY_METER == 1
    #warning "Energy meter disabled!"
#endif

#if OVERRIDE_WIFI == 1
    #warning "WiFi override enabled!"
#endif

#if DEVICE_DEBUGGING == 1
    #warning "Device debugging enabled!"
#endif

#if MEMORY_DEBUGGING == 1
    #warning "Memory debugging enabled!"
#endif


namespace _2log
{
    BaseDevice::BaseDevice() : IDFix::Task("update_task"), _deviceMutex(Mutex::Recursive), _settings(), _wifiManager(this)
    {
        #if SYSTEM_MONITORING == 1
            _systemMonitor.start();
        #endif

        esp_log_level_set("*", DEVICE_LOG_LEVEL );
    }

    void BaseDevice::startDevice()
    {
        esp_err_t result = nvs_flash_init();

        #ifdef CONFIG_IDF_TARGET_ESP32
            if (result == ESP_ERR_NVS_NEW_VERSION_FOUND)
            {
              ESP_ERROR_CHECK( nvs_flash_erase() );
              result = nvs_flash_init();
            }
        #endif

        if (result == ESP_ERR_NVS_NO_FREE_PAGES)
        {
            ESP_ERROR_CHECK( nvs_flash_erase() );
            result = nvs_flash_init();
        }

        ESP_ERROR_CHECK(result);

        #if ALLOW_3RD_PARTY_FIRMWARE == 1
            ESP_LOGW(LOG_TAG, "Running an unsecured firmware!");
        #else
            ESP_LOGI(LOG_TAG, "Running a secured firmware!");
        #endif

        _settings.init();
        _wifiManager.init();

        #if OVERRIDE_CONFIG == 1

            // if we set override config, skip configuration steps
            setupConnections();

        #else
            if ( _settings.isConfigured() == false)
            {
                ESP_LOGW(LOG_TAG, "Device is NOT configured");
                startConfiguration();
            }
            else
            {
                ESP_LOGI(LOG_TAG, "Device is configured");
                setupConnections();
            }
        #endif
    }

    std::string BaseDevice::getDeviceID() const
    {
        return IDFix::WiFi::WiFiManager::getStationMACAddress();
    }

    void BaseDevice::resetDeviceConfigurationAndRestart()
    {
        ESP_LOGW(LOG_TAG, "Reset device configuration and restart...");
        _settings.clearConfig();
        esp_wifi_disconnect ();
        esp_wifi_stop();
        esp_wifi_deinit();
        esp_restart();
    }

    int8_t BaseDevice::getRSSI() const
    {
        return _wifiManager.getRSSILevel();
    }

    BaseDevice::BaseDeviceState BaseDevice::getState() const
    {
        return _deviceState;
    }

    void BaseDevice::run()
    {
        performUpdate();
    }

    void BaseDevice::startConfiguration()
    {
        _deviceState = BaseDeviceState::StartConfiguring;
        baseDeviceStateChanged(_deviceState);

        bool    configurationInitiated;
        int     retryCount = 0;

        _wifiManager.setCertificate(deviceCertificate, deviceCertificateSize);
        _wifiManager.setPrivateKey(deviceKey, deviceKeySize);
        _wifiManager.addConfigDeviceParameter("sid", _settings.getShortID() );
        _wifiManager.addConfigDeviceParameter("uuid", getDeviceID() );

        do
        {
            configurationInitiated = _wifiManager.startConfiguration(CONFIGURATION_WIFI_SSID, CONFIGURATION_WIFI_PWD);
            if ( ! configurationInitiated )
            {
                ESP_LOGE(LOG_TAG, "Failed to initiate config mode");
                IDFix::Task::delay(CONFIGURATION_RETRY_DELAY);
                retryCount++;
            }
            else
            {
                ESP_LOGI(LOG_TAG, "Initiated config mode");
            }
        }
        while ( ! configurationInitiated && retryCount < CONFIGURATION_MAX_RETRY);

        if ( ! configurationInitiated )
        {
            ESP_LOGE(LOG_TAG, "Finally failed to initiate config mode: giving up and reboot...");
            _deviceState = BaseDeviceState::Booting;
            baseDeviceStateChanged(_deviceState);
            esp_restart();
        }
    }

    void BaseDevice::configurationStarted()
    {
        _deviceState = BaseDeviceState::Configuring;
        baseDeviceStateChanged(_deviceState);
        baseDeviceEventHandler( BaseDeviceEvent::ConfigurationStarted );
        ESP_LOGI(LOG_TAG, "Started config mode waiting for config device...");
    }

    void BaseDevice::receivedWiFiConfiguration(const std::string &ssid, const std::string &password)
    {
        if ( _deviceState == BaseDeviceState::Configuring)
        {
            _settings.setWiFiSSID(ssid);
            _settings.setWiFiPassword(password);
        }
    }

    void BaseDevice::receivedConfigurationParameter(const std::string &param, const std::string &value)
    {
        if ( _deviceState == BaseDeviceState::Configuring)
        {
            ESP_LOGI(LOG_TAG, "receivedConfigurationParameter( %s, %s )", param.c_str(), value.c_str() );

            if ( param == SERVER_CONFIG_PARAMETER )
            {
                _settings.setServerURL(value);
            }
        }
    }

    void BaseDevice::receivedConfigurationParameter(const std::string &param, const bool value)
    {
        if ( _deviceState == BaseDeviceState::Configuring)
        {
            ESP_LOGI(LOG_TAG, "receivedConfigurationParameter( %s, %d )", param.c_str(), value );

            if ( param == TEST_CONFIG_PARAMETER )
            {
                _testReceivedWifiConfig = value;
            }
        }
    }

    void BaseDevice::configurationFinished()
    {
        ESP_LOGI(LOG_TAG, "Configuration Finished: Saving config and rebooting to test config...");
        _settings.saveConfiguration();
        _deviceState = BaseDeviceState::Booting;
        baseDeviceStateChanged(_deviceState);
        baseDeviceEventHandler(BaseDeviceEvent::ConfigurationSucceeded);

        DeviceProperties::instance().saveProperty(".configReset", _testReceivedWifiConfig);

        #if ALLOW_3RD_PARTY_FIRMWARE == 1

            ESP_LOGW(LOG_TAG, "First 2log configuration on an open firmware! Switching to secured firmware!");
            IDFix::FOTA::FirmwareUpdater::activateNextUpdatePartition();

        #endif

        esp_restart();
    }

    void BaseDevice::networkDisconnected()
    {
        _deviceMutex.lock();
            _networkConnected = false;
        _deviceMutex.unlock();

        ESP_LOGW(LOG_TAG, "WiFi disconnected! (running in task %s)", Task::getRunningTaskName().c_str() );
        bool success;

        if( DeviceProperties::instance().getProperty(".configReset", false).asBool(&success) && success)
        {
            // this was the first start after a configuration and wifi connection was not successful
            // we asume the configuration was incorrect, clear the configuration and restart in configuration mode again

            resetDeviceConfigurationAndRestart();
        }
        else
        {
            connectWiFi();
        }
    }

    void BaseDevice::networkConnected(const IDFix::WiFi::IPInfo &ipInfo)
    {
        DeviceProperties::instance().saveProperty(".configReset", false);

        _deviceMutex.lock();
            _networkConnected = true;
            _deviceNodeConnectionRetries = 0;
        _deviceMutex.unlock();

        ESP_LOGI(LOG_TAG, "networkConnected - IP: " IPSTR, IP2STR(&ipInfo.ip) );

        baseDeviceEventHandler(BaseDeviceEvent::NetworkConnected);

        _deviceNode->connect();
    }

    void BaseDevice::deviceNodeConnected()
    {
        ESP_LOGI(LOG_TAG, "deviceNodeConnected");

        _deviceMutex.lock();
            _deviceNodeConnectionRetries = 0;
        _deviceMutex.unlock();

        baseDeviceEventHandler(BaseDeviceEvent::NodeConnected);

        _deviceMutex.lock();
            _deviceState = BaseDeviceState::Connected;
        _deviceMutex.unlock();

        baseDeviceStateChanged(_deviceState);
    }

    void BaseDevice::deviceNodeDisconnected()
    {
        ESP_LOGW(LOG_TAG, "DeviceNode disconnected (running in task %s)", Task::getRunningTaskName().c_str() );

        uint32_t delayTime = 0;

        MutexLocker locker(_deviceMutex);

        if ( _networkConnected )
        {
            baseDeviceEventHandler(BaseDeviceEvent::NodeDisconnected);

            _deviceNodeConnectionRetries++;

            if ( _deviceNodeConnectionRetries > CONNECTION_RETRY_LIMIT_UNTIL_WIFI_RECONNECT )
            {
                ESP_LOGE(LOG_TAG, "Device node reconnection limit (WiFi) reached!");
                //ESP_LOGE(LOG_TAG, "Device node reconnection limit (WiFi) reached! Restart WiFi...");

                // TODO: Implement disconnect in WiFiManager
                // TODO: disconnect and reconnect WiFi
            }

            if ( _deviceNodeConnectionRetries > CONNECTION_RETRY_LIMIT_UNTIL_DELAY )
            {
                delayTime = CONNECTION_RETRY_DELAY_TIME;
            }

            ESP_LOGI(LOG_TAG, "Trying to reconnect deviceNode...");

            _deviceState = BaseDeviceState::Connecting;

            locker.unlock();

            baseDeviceStateChanged(_deviceState);

            _deviceNode->connect(delayTime);
        }
    }

    void BaseDevice::deviceNodeAuthKeyChanged(uint32_t newAuthKey)
    {
        _settings.writeAuthKey(newAuthKey);
    }

    void BaseDevice::initProperties(cJSON *argument)
    {
        cJSON_AddStringToObject(argument, ".ip",         WiFi::IPInfo::ipToString( getIPInfo().ip ).c_str() );
        cJSON_AddStringToObject(argument, ".mac",        WiFi::WiFi::getStationMACAddress().c_str() );
        cJSON_AddNumberToObject(argument, ".rssi",       getRSSI());
        cJSON_AddNumberToObject(argument, ".fwstatus",   static_cast<int>(FirmwareState::Stable) );
    }

    bool BaseDevice::connectWiFi()
    {
        baseDeviceEventHandler(BaseDeviceEvent::NetworkConnecting);

        _deviceState = BaseDeviceState::Connecting;
        baseDeviceStateChanged(_deviceState);

        #if OVERRIDE_WIFI == 1
            ESP_LOGI(LOG_TAG, "Connection to ssid: %s", WIFI_SSID);
            return _wifiManager.connectWPA(WIFI_SSID, WIFI_PASSWORD, ENABLE_WIFI_PMF);
        #else
            ESP_LOGI(LOG_TAG, "Connecting to ssid: %s", _settings.getWiFiSSID().c_str() );
            return _wifiManager.connectWPA(_settings.getWiFiSSID(), _settings.getWiFiPassword(), ENABLE_WIFI_PMF);
        #endif
    }

    void BaseDevice:: setupConnections()
    {
        ESP_LOGI(LOG_TAG, "Device configured and starts running");

        std::string connectionURL;

        #if OVERRIDE_CONFIG == 1
            connectionURL = SERVER_URL;
        #else
            connectionURL = _settings.getServerURL();
        #endif

        _deviceNode = new DeviceNode( new Connection( connectionURL, ROOT_CA ), this, DEVICE_TYPE, getDeviceID(), _settings.getShortID(), _settings.getAuthKey() );

        _deviceNode->registerInitPropertiesCallback(std::bind(&BaseDevice::initProperties, this, std::placeholders::_1) );
        _deviceNode->registerRPC(".fwupdate",       std::bind(&BaseDevice::updateFirmwareRPC, this, std::placeholders::_1) );

        connectWiFi();

        #if DUMP_TASK_STATS == 1
            while (true)
            {
                IDFix::Task::delay(5000);
                IDFix::TaskManager::printTaskList();
            }
        #endif
    }

    void BaseDevice::updateFirmwareRPC(cJSON *argument)
    {
        ESP_LOGI(LOG_TAG, "Firmware update triggered (running in task %s)", Task::getRunningTaskName().c_str() );

        if(_deviceState == BaseDeviceState::UpdatingFirmware)
        {
            ESP_LOGI(LOG_TAG, "Update already in progress..");
            return;
        }

        cJSON *urlValItem = cJSON_GetObjectItem(argument, "val");
        if ( cJSON_IsString(urlValItem) )
        {
            _stateBeforeFirmwareUpdate = _deviceState;
            baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateStarted);
            _deviceState = BaseDeviceState::UpdatingFirmware;
            baseDeviceStateChanged(_deviceState);

            ESP_LOGI(LOG_TAG, "Update URL: %s", urlValItem->valuestring);
            _deviceNode->setProperty(".fwstatus", static_cast<int>(FirmwareState::InitUpdate) );
            _updateURL = urlValItem->valuestring;

            startTask();
        }
        else
        {
            ESP_LOGE(LOG_TAG, "cJSON_IsString(urlValItem) failed");
            _deviceNode->setProperty(".fwstatus", static_cast<int>(FirmwareState::InvalidUpdateArgument) );
        }
    }

    void BaseDevice::performUpdate()
    {
        ESP_LOGI(LOG_TAG, "Performing firmware update from URL %s", _updateURL.c_str() );

        FOTA::FirmwareUpdater           updater;
        FOTA::HTTPFirmwareDownloader    downloader;
        Crypto::PublicKey               publicKey;
        Crypto::HashSHA256              hashSHA256;
        Crypto::ECDSASignatureVerifier  signatureVerifier;

        int result;

        if ( (result = publicKey.parseKey(firmwareSignaturePublicKey, firmwareSignaturePublicKeyLength) ) != 0 )
        {
            ESP_LOGE(LOG_TAG, "Failed to parse public key!");
            baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateFailed);
            _deviceState = _stateBeforeFirmwareUpdate;
            baseDeviceStateChanged(_deviceState);
            return;
        }

        if ( signatureVerifier.setPublicKey(&publicKey) != 0 )
        {
            ESP_LOGE(LOG_TAG, "Failed to set public key for verifier!");
            baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateFailed);
            _deviceState = _stateBeforeFirmwareUpdate;
            baseDeviceStateChanged(_deviceState);
            return;
        }

        updater.installSignatureVerifier(&signatureVerifier, &hashSHA256);

        if ( ! updater.setMagicBytes(FIRMWARE_MAGIC_BYTES, strlen(FIRMWARE_MAGIC_BYTES)) )
        {
            ESP_LOGE(LOG_TAG, "Failed to setMagicBytes. Aborting...");
            baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateFailed);
            _deviceState = _stateBeforeFirmwareUpdate;
            baseDeviceStateChanged(_deviceState);
            return;
        }

        downloader.setFirmwareWriter( &updater );

        esp_http_client_config_t httpConfig;
        memset(&httpConfig, 0, sizeof(esp_http_client_config_t) );
        httpConfig.url = _updateURL.c_str();

        if ( updater.beginUpdate() )
        {
            _deviceNode->setProperty(".fwstatus", static_cast<int>(FirmwareState::UpdateDownloading) );

            if ( downloader.downloadFirmware(&httpConfig) == 0 )
            {
                if ( updater.finishUpdate() )
                {
                    ESP_LOGI(LOG_TAG, "Firmware update successful. Restarting...");
                    _deviceNode->setProperty(".fwstatus", static_cast<int>(FirmwareState::UpdateSucceeded) );
                    baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateSucceeded);
                    Task::delay(3000);
                    esp_restart();
                }
                else
                {
                    ESP_LOGE(LOG_TAG, "Failed to finish firmware update...");
                    _deviceNode->setProperty(".fwstatus", static_cast<int>(FirmwareState::UpdateFailed) );
                    baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateFailed);
                    _deviceState = _stateBeforeFirmwareUpdate;
                    baseDeviceStateChanged(_deviceState);
                    Task::delay(3000);
                    esp_restart();
                }
            }
            else
            {
                ESP_LOGE(LOG_TAG, "Failed to download firmware...");
                _deviceNode->setProperty(".fwstatus", static_cast<int>(FirmwareState::UpdateDownloadFailed) );
                baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateFailed);
                _deviceState = _stateBeforeFirmwareUpdate;
                baseDeviceStateChanged(_deviceState);
                updater.abortUpdate();
            }
        }
        else
        {
            ESP_LOGE(LOG_TAG, "Failed to start update...");
            _deviceNode->setProperty(".fwstatus", static_cast<int>(FirmwareState::UpdateStartFailed) );
            baseDeviceEventHandler(BaseDeviceEvent::FirmwareUpdateFailed);
            _deviceState = _stateBeforeFirmwareUpdate;
            baseDeviceStateChanged(_deviceState);
        }
    }

    void BaseDevice::baseDeviceEventHandler(BaseDeviceEvent event)
    {
        ESP_LOGD(LOG_TAG, "Default baseDeviceEventHandler triggered");
    }

    void BaseDevice::baseDeviceStateChanged(BaseDeviceState state)
    {
        ESP_LOGD(LOG_TAG, "Default baseDeviceStateChanged handler triggered");
    }

    IDFix::WiFi::IPInfo _2log::BaseDevice::getIPInfo() const
    {
        return _wifiManager.getStationIPInfo();
    }

}
